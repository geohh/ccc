\section{From Fourier Series to the Poisson Kernel on a Disk}

One ubiquitous object in the study of potentials is the Laplace equation
\begin{equation}
  \Delta u = \sum_{k=1}^d \partial_k^2 u = 0
\end{equation}
In fact, the term \emph{Potential Theory} was introduced based on the \nth{19}
century realization that potential functions solving Laplace's equation
could be used to generalize problems in oribital mechanics and electromagnetics.
Consideration of the Laplace equation need not be restricted to two dimensions.
In general, a conservative vector field has divergence equal to a physical,
internal concentration, of something like charge\cite{schlag2014}.
The conservative field is equal to the gradient of the potential, hence
the Poisson equation and its homogenous variant Laplace's equation.
In two dimensions, however, this connects to the study of complex functions
in an important way: the Cauchy-Reimann equations require that for a function
$f(x + iy) = u(x,y) + i v(x,y)$ to be holomorphic,
both $u$ and $v$ must both be part of the class of fundamental solutions to
Laplace's equation.

We consider a problem where a potential is forced on the edge of a disk $D_R$
to some known function $\aleph(\phi;R)$, and we wish to find the quilibrium values
that satisfy Laplace's equation within\sidenote{Note that $\aleph$ must satisfy
Laplace's equation itself, even if we consider the open disk, to avoid
discontinuities.} this disk.
A helpful genie might know a function $f(z=re^{i\phi})$, of which $\aleph$ is 
the real part of a complex boundary condition.
We want to find the solution, then,\sidenote{The factor of $2$ is
moved for convenience of notation.}
\begin{equation}
  u(r,\phi) = 2 \Re(f(z)) = f(z) + \conj{f(z)}
\end{equation}
The boundary $\partial D_R$ of the disk is a limited continuum, so it's reasonable to
think that functions along it like $\aleph$ have Fourier series representing them
as a sequence of coeffienents $(c_k)$ such that
\begin{equation}
  \aleph(\phi) = \frac{1}{\sqrt{2 \pi}} \sum_k c_k \exp(i k \phi)
\end{equation}

\newthought{In fact, there is} an intimate connection between functions with
convergent Fourier series and solvable boundary value problems for Laplace's
equation.
As noted, a complex-valued augmented solution $f(z)$ must be analytic, that is,
it has a Taylor series, about the origin.
\begin{equation}
  f(z) = \sum_k \alpha_k z^k = \sum_k \frac{D^kf(0)}{k!} z^k
\end{equation}
So, along a circle of radius $R$,
\begin{equation}
  f(R e^{i \theta}) = \sum_k \alpha_k R^k e^{i k \theta}
\end{equation}
So, if $\aleph(\phi)$ is a valid boundary condition on the circle for Laplace's equation,
then it must have a convergent Fourier series.
One may note that if we now used Fourier series to find Laplace equation 
solutions, we would be employing a circular argument.
This is true!
However, one may look at other proofs of conditions on Fourier series convergence
to justify an entry point of this discussion, and think of this as an intuitive
connection.



\begin{figure}
  \includegraphics[width=\textwidth]{figures/build/poisson_kernel.pdf}

  \caption{Integrating the Laplace equation for the boundary data $g(\phi) = \sin(2 t)$ on the unit circle.}
\end{figure}

\newthought{We know from linear algebra} that we can expand an object in an
orthonormal basis by repeatedly projecting.
For this Fourier basis, we might define an inner product space as an intuitive
guess, and guess that we can read components of a Fourier series off via
projection.
\begin{equation}
  \langle f, g \rangle = \int_\mathbb{T} f(x) \conj{g(x)} dx
\end{equation}
\begin{equation}
  c_k = \frac{1}{\sqrt{2\pi}} \int_\mathbb{T} f(x) e^{-ikx} dx
\end{equation}

We would like to consider the sequence $s_n(x)$ of Fourier series approximations
of a function. Interestingly, it turns out to be equivalent to the sequence
of convolutions of the function with the \emph{Dirichlet-Dini kernel}.
\begin{align}
  s_n(x)
  &= \frac{1}{\sqrt{2\pi}} \sum_{k=-n}^{n} \int_{\mathbb{T} } f(y) e^{-iky} dy e^{ikx} \\
  &= \frac{1}{\sqrt{2\pi}} \int_{\mathbb{T}} f(y) \left( \sum_{k=-n}^{n} e^{ik(x-y)} \right) dy \\
  &= (f \star D_n)(x)
\end{align}
We define the Dirichlet-Dini kernel here as $D_n(x)$\sidenote{The factor of $\frac{1}{2\pi}$
is absorbed into the definition of the periodic convolution operator.}
and expand it as a geometric series.
\begin{align}
  D_n(x)
  &= \sum_{k=-n}^{n} e^{ikx} \\
  &= \sum_{k=0}^{n} e^{ikx} + e^{-inx} \sum_{k=0}^{n-1} e^{ikx} \\
  &= \frac{1 - e^{ix(n+1)}}{1-e^{ix}} + \frac{1-e^{inx}}{1-e^{ix}} \\
  &= \frac{\exp(-ix(n+\frac{1}{2})) - \exp(ix(n+\frac{1}{2}))}{\exp(-in\frac{1}{2}) - \exp(in\frac{1}{2})} \\
  &= \frac{\sin((n+\frac{1}{2})x)}{\sin(\frac{x}{2})}
\end{align}

\begin{figure}\label{dirichlet_kernel}
  \includegraphics[width=\textwidth]{figures/build/dirichlet_kernel.pdf}

  \caption{Dirichlet kernels, $n=1, 4, 7$.}
\end{figure}

As visualized (Fig.~\ref{dirichlet_kernel}), increasing $n$ create kernels 
$D_n(x)$ that look increasingly like the Dirac delta distribution
$\delta(x)$\sidenote{This is true on a single period. Going through a similiar
argument for a non-periodic Fourier transform, a different, non-periodic
sinc kernel can be used.}.
Periodic convolution with a periodic delta is equivalent to the identity
operator, so it makes intuitive sense that $s_n$ converges to $f$.
More specifically, we can explore an instance of \emph{Bessel's inequality}
based on the error of a truncated Fourier series for an $L_2$-integrable
function.
\begin{align*}
  0 &\leq \norm{f(x) - s_n(x)}_2^2 \\
  &= \norm{f(x)}_2^2 - 2 \sum_{k=0}^{n} \Re \inp{f(x)}{\inp{f(x)}{e^{-ikx} e^{ikx}}} + \sum_{k=0}^{n} \abs{\inp{f(x)}{e^{ikx}}}^2 \\
  &= \norm{f(x)}_2^2 - \sum_{k=0}^{n} \abs{\inp{f(x)}{e^{ikx}}}^2
\end{align*}
\begin{equation}
  \frac{1}{2\pi} \sum_{k=0}^{n} c_k \leq \norm{f(\cdot)}_2^2
\end{equation}
This can be related to Parseval's relation.
Additionally, we note that $c_k$ must necessarily go to zero, to have
a finite sum.
And finally, this statement can be considered to show the general identity
that the inner products of a pure tone and a integrable function goes
to zero in the high-frequency limit.

\newthought{However, despite the} Dirichlet kernel looking more and more
like a Dirac delta for large $n$, it does not converge to it in any important
mathematical sense.
We must, instead, consider the sequence for the errors of successive Fourier
approximations.
\begin{align}
  \abs{s_n(x) - f(x)}
  &= \abs{ \frac{1}{2\pi} \int_\mathbb{T} (f(x+y) - f(x)) D_n(y) dy } \\
  &= \abs{ \frac{1}{2\pi} \int_\mathbb{T} g(y) \sin((n+\frac{1}{2})y) dy } \\
\end{align}
where $g(y)$ is not properly defined but has a bi-directional limit by L'H\^{o}pital's
(which is what the integral cares about) around $y=0$, so long as $f$ is differentiable.
Thus, the Fourier series must converge.

\newthought{With a connection established} between Fourier series and
Laplace equation solutions, and a separate on-ramp to the Fourier series,
we can derive a nice integral kernel to solve the Laplace equation.
The complex solution $f( r e^{i\theta} )$ must have a Taylor series $\alpha_k$, so for
the real solution $u(z) = f(z) + \conj{f(z)}$,
\begin{equation}
  u( r e^{i \theta}) = (\alpha_0 + \conj{\alpha_0}) + \sum_{k>0} \alpha_k r^k \exp(ik\theta) + \sum_{k<0} \conj{\alpha_k} r^k \exp(-ik\theta)
\end{equation}
where $\alpha_k = \frac{c_k}{R^k}$ from the Fourier series of the boundary data.
For simplicity of notation, let $R=1$.
Noting that periodic convolution is diagonalized by taking the Fourier

Specifically,
\begin{equation}
  g(Re^{i\theta}) = \aleph(\theta) = \sum_k c_k \exp(i k \theta) = \sum_k \alpha_k R^k \exp(i k \theta)
\end{equation}
\begin{align}
  K(\theta; r)
  &= 1 + \sum_{k > 0} \frac{r^k}{R^k} \exp(i k \theta) + \sum_{k < 0} \frac{r^k}{R^k} \exp(-i k \theta) \\
  &= 1 + 2 \Re \sum_{k>0} \frac{z^k}{R^k} \\
  &= 1 + 2 \Re \frac{z}{R-z} \\
  &= 2 \Re(\frac{z}{R-z} + \frac{1}{2}) \\
  &= \Re \frac{R+z}{R-z} \\
  &= \frac{R^2 - r^2}{R^2 - 2Rr\cos\theta + r^2}
\end{align}
% \newcommand{\ostar}{\mathbin{\mathpalette\make@circled\star}}
\begin{equation}
  u(z) = (g \star K)(z)
\end{equation}
for a circular convolution operator $\star$ around the complex circle.
Transforming the convolution back from Fourier to conventional domain,
we can write a solution to the problem
\begin{equation}
  u(r,\theta) = \frac{1}{2\pi} \int_0^{2\pi} \frac{R^2 - r^2}{R^2 - 2Rr\cos(\theta - \varphi)} \aleph(\varphi) d \varphi
\end{equation}

\newthought{As noted previously,} there exist conformal maps from the
unit disk to the half-plane by Mobius transforms.
Specifically, we'll use a map called the \emph{Cayley transformation}.
\begin{equation}
  \beth(z) = \frac{z+i}{iz+1}
\end{equation}
By inspection, this transform is invertible.

\begin{figure}

  \includegraphics[width=\textwidth]{figures/build/disk_to_halfplane.pdf}

  \caption{The Cayley transformation maps points inside the unit disk
  into the positive imaginary half-plane.}

\end{figure}

Now, take the traditional formula and substitute $z \mapsto \beth^{-1}(z)$,
$e^{i \phi} \mapsto \frac{t-i}{-it+1}$, and $d\theta = d(e^{i\theta})/i e^{i \theta}$.
We have a formula for the upper half-plane\cite{garrett2016}
\begin{equation}
  (u \circ \beth^{-1})(z) = \frac{1}{2\pi} \int_\mathbb{R} g(e^{i \theta})(t) K(\beth^{-1}(t),\beth^{-1}(z)) \frac{d(e^{i\theta})}{i e^{i \theta}}
\end{equation}
We can simplify this piece-by-piece.
\begin{equation*}
  \frac{d(\beth^{-1}(t))}{i \beth^{-1}(t)} = \frac{2 dt}{1+t^2}
\end{equation*}
\begin{equation*}
  K_\mathfrak{h}(t, x+iy) = \frac{\abs{1-it}^2y}{(x-t)^2 + y^2}
\end{equation*}
\begin{equation}
  u(x,y) = \frac{1}{\pi} \int_\mathbb{R} g(t) \frac{y}{(x-t)^2 +y^2} dt
\end{equation}

\begin{figure}

  \includegraphics[width=\textwidth]{figures/build/halfplane_laplace.pdf}

  \caption{Solving the Laplace equation on the half-plane, with boundary
  data $g(x) = \sin(x) + \sin(3x)$.}

\end{figure}
