\section{Mobius Transforms and the Smith Chart}

When talking about electrical signals at low frequencies\sidenote{While there
is no fixed cutoff where we begin to describe frequencies as ``radio,'' commercial
ADCs may easily be found as of writing with sampling rates above 10E9 samples
per second. Thus, a fast but not unreasonably fast wave to sample above the
Nyquist limit is 5GHz.} it makes sense to talk about the time-varying values
of a signal.
However, when dealing with faster waves, we find that other measurements
become more natural to take.
The power of a wave can be measured to indirectly compute amplitude,
and similiarly, we may find it less natural to measure input impedance, and more
natural to measure a \emph{reflection factor}.
The reflection factor is defined by the relation
\begin{equation}
\Gamma = \frac{E\,\text{reflected}}{E\,\text{traveling forward}}
\end{equation}

We would still like, however, to relate RF-friendly quantities to quantities
that we as circuit designers are more used to --- one such graphical
representation is the Smith Chart, which is based on the
observation\cite{caspers2013} that
\begin{equation}
  \Gamma = \frac{Z - Z_0}{Z + Z_0}
\end{equation}

This relation is one of a general form known as \emph{bilinear transforms} or
\emph{Möbius transforms}.
Here $Z_0$ is a reference impedance that we use to normalize other impedance
values.
Using this normalization as intended, we can also write $\Gamma$ in terms of a
unitless impedance $z = Z / Z_0$:
\begin{equation}
  \Gamma = \frac{z-1}{z+1}
\end{equation}

\begin{figure}
  \includegraphics{figures/build/Z_to_Gamma.pdf}

  \caption{Transforming three lines (representing a resistor, capacitor, and inductor)
  from $Z$ to $\Gamma$ gives us the overall structure that the Smith Chart
  visualization aid takes.}
\end{figure}

And yet another valuable quantity in RF electronics in the so-called
\emph{Voltage-domain Standing Wave Ratio (VSWR)}, the ratio of minimum and
maximum amplitudes when the forward and reflected wave components off a device
settle into a single standing wave.
In the case of an ideal source driving a reflective load, VSWR is defined
\begin{equation}
  \mathrm{VSWR} = \frac{1 + \Gamma}{1 - \Gamma}
\end{equation}

\newthought{The ubiquity of} Möbius transforms in RF circuits encourages us to
study them generally and discover some properties we can apply to these and
other relations.
In general, we can define a Möbius transform as
\begin{equation}
  f(z) = \frac{az + b}{cz + d}
\end{equation}
where $ad - bc \not= 0$.
This constraint is imposed because if a function is meant to act as a
``transform'' between two bases of variable, we would like it to be bijective.
Where $ad - bc = 0$, however, we note that
\begin{align}
  f(z) &= \frac{c}{c} \frac{az + b}{cz + d} \\
  &= \frac{caz + bc}{c(cz+d)} \\
  &= \frac{a(cz+d)}{c(cz+d)} \\
  &= \frac{a}{c}
\end{align}
which is a constant number, so we cannot make a reversible transform.
However, this is only one counterexample.
There is another problem with treating this formulation: the pole.
Projecting, a Möbius transform $\abs{f(z)}$ diverges as $z \to -\frac{d}{c}$.
However, the fact that it may have only a single singularity lets us get around
this problem by simply redefining this function's domain and co-domain.
The \emph{extended complex plane} or \emph{Reimann sphere}, which I will denote
$\mathbb{C}_\infty$, is the complex plane, plus the ``number'' infinity, and
algebraic relations connecting them.
Normally, these are defined\cite{wiki:riemann_sphere},
where $z \in \mathbb{C}$, as
\begin{align*}
  z + \infty = \infty && z \cdot \infty = \infty \\
  \frac{z}{0} = \infty && \frac{z}{\infty} = 0 \\
\end{align*}
The Mobius transform $f(z)$ takes on $f(\infty) = \frac{a}{c}$
and $f(-\frac{d}{c}) = \infty$, making it a bijection
$f: \mathbb{C}_\infty \mapsto \mathbb{C}_\infty$.\sidenote{Note that you can
either view the extension of mobius transforms as a consequence of the
algebraic properties above, or the algebraic properties as consequences of
defining appropriate extended Mobius transforms.}

\newthought{The careful reader} may have experienced a moment of deja vu
to an introductory Linear Algebra class above,
when it was noted that a class of transformation has special behavior
for $ad - bc = 0$.
This is no mere coincidence of notation, as we will now discuss Mobius
transforms as a \emph{matrix group}.\sidenote{I am unsure whether it makes more
sense to mark this as \emph{vocabulary} or ``speaking loosely,'' as, while
it is correct, I will treat it very non-rigorously.}
Consider a ratio of complex numbers represented by a vector in $\mathbb{C}_\infty^2$, and a linear transformation
\begin{align*}
  z \equiv \frac{x}{y} \sim \begin{pmatrix} x \\ y \\ \end{pmatrix},
    &&
    f(z) \equiv \frac{az+b}{cz+d} \sim T = \begin{pmatrix} a & b \\ c & d \\ \end{pmatrix}
\end{align*}
We can relate the actions of these maps on their respective objects:
\begin{align}
  f(z) = \frac{a \left( \frac{x}{y} \right) + b}{c \left( \frac{x}{y} \right) + d} = \frac{ax + by}{cx + dy}
  &&
  T \begin{pmatrix} x \\ y \\ \end{pmatrix} = \begin{pmatrix} ax + by \\ cx + dy \\ \end{pmatrix}
\end{align}

In order to make a matrix group out of this representation,
we must also show it to be closed under composition.
Letting
\begin{align*}
  f(z) = \frac{az+b}{cz+d} && g(z) = \frac{\alpha z + \beta}{\gamma z + \delta}
\end{align*}
\begin{align}
  (f \cdot g)(z) &= \frac{a \frac{\alpha z + \beta}{\gamma z + \delta} +b}{c \frac{\alpha z + \beta}{\gamma z + \delta} +d} \\
  &= \frac{(a \alpha + b \gamma) z + (a \beta + b \delta)}{(c \alpha + d \gamma) z + (c \beta + d \delta)}
\end{align}
So, composing Mobius transforms is equivalent to contracting their matrix representations.
Since $2 \times 2$ complex matrices are closed under matrix-multiplication,
we have shown that our Mobius transformations are a group, and isomorphic to
this matrix group which may be used to find some valuable things
\sidenote{If you happen to use this to take the \emph{eigenvalues} of the
Mobius transform, well, I guess I'll see you last year.}.

\newthought{One such property} is the inverse of a Mobius transform, which is provable
but unintuitive\sidenote{perhaps only to my small brain\textellipsis} without the matrix representation.
With the matrix representation, however, it is quite direct.
There is a well-known formula to invert a $2 \times 2$ matrix, and we know
that composition of Mobius transforms is equivalent to matrix-multiplication,
so finding the inverse of the matrix representation is equivalent to finding the
inverse of a transform.
\begin{equation}
  f^{-1}(z) = \frac{1}{ad-bc} \frac{d z - b}{-c z + a}
\end{equation}

We can also identify some geometrically important Mobius transformations.
\begin{enumerate}
  \item $z \mapsto z + z_0$: translating a point by $z_0$.
  \item $z \mapsto \frac{1}{z} = \frac{\bar{z}}{\abs{z}^2}$:
    reflecting a point about the real axis, and then the unit circle.
  \item $z \mapsto \alpha \exp(i \phi) z$: rotating and scaling a point.
\end{enumerate}
\begin{figure}
  \includegraphics{figures/build/one_over_z.pdf}
  \caption{The effect of the transformation $z \mapsto \frac{1}{z}$.
  Points share a color with their counterpart under the transformation.
  The unit circle is marked in black.
  }
\end{figure}
From the geometric meaning of these steps, we know that they are \emph{conformal},
that they, locally, conserve the angles between points.
Consider the example mapping $\Gamma = \frac{z-1}{z+1}$: despite the obvious
macro-scape differences in the shapes of images of lines, locally, we find that
the images of the inductor- and capacitor-like paths are smooth and do
not have angles between points.
This is also true of general Mobius transformations, since we can
construct\cite{vica_mobius}:
\begin{enumerate}
  \item $f_1(z) = z + \frac{d}{c}$
  \item $f_2(z) = \frac{1}{z}$
  \item $f_3(z) = \frac{ad-bc}{c^2} z$
  \item $f_4(z) = z + \frac{a}{c}$
\end{enumerate}
\begin{equation}
  (f_4 \cdot f_3 \cdot f_2 \cdot f_1)(z) = \frac{az+b}{cz+d}
\end{equation}
Since a general Mobius transform may be composed from geometric, conformal stages,
we know that general Mobius transforms also locally conserve angle.
There are two types of shapes that are infinite, and smooth enough to be locally flat:
circles, and lines.
Thus, we intuit with no math that Mobius transforms will preserve ``generalized circles''
where a circle of infinite radius is a line.

\newthought{The preservation of lines and circles} leads us to the concept of \emph{Gain Circles},
and \emph{Stability Circles}, where a figure of merit of an RF circuit depends on the load impedence
it's attached to, and we can draw a circle of it as the load varies.
For instance, as we vary the load of an amplifier, we can write the effective source-facing
reflection of an amplifier\cite{hp:s-params}
\begin{equation}
  \Gamma = s_{11} + \frac{ s_{12 }s_{21} \Gamma_L }{ 1 - s_{22} \Gamma_L }
\end{equation}
First, note that this is a Mobius transform of $\Gamma_L$, where $s_{ij}$ are held constant.
If the overall reflection is outside of the unit circle\sidenote{This is only physically possible
when the gain is being reflected back through the load.} the source will oscillate.
If we, the engineers, can guarantee that an amplifier is seeing a certain load reflection,
we can design around placing a certain point of this stability circle.

