\section{Integral Complex Calculus}

For the real integral
\begin{equation*}
  \int_a^b f(x) dx
\end{equation*}
there is one path from $a$ to $b$\sidenote{and one path in the opposite
direction, which relates integrals in an important way}.
However, a similiar statement for complex functions
\begin{equation*}
  \int_a^b f(z) dz
\end{equation*}
makes no sense --- which of the infinite paths from $a$ to $b$ are we taking?
In what direction is the differential $dz$?

Complex integrals must be constructed out of real integrals\sidenote{The
analysis-minded reader may ask if it makes a difference whether we integrate
in the form of Reimann or Legensque --- similiar to real analysis, it only makes
a difference in certain, very special cases.}
along a \emph{contour} $\gamma$, a parametric path.
\begin{equation}
  \int_{\gamma} f(z) dz = \int_a^b f(\gamma(t)) (D\gamma) (t) d t
\end{equation}

\newthought{On the real line} it seems intuitive and almost useless that
integrals are ``reversible,'' that we can backtrack the path of an integral
and reliably get the opposite result.
However, complex contour integrals offer an infinitely richer set of paths
from $a$ to $b$.
An important result from complex analysis is that we can deform the path $\gamma$
smoothly across a differentiable field\sidenote{I use the word in the same
sense here as in a physical \textit{field theory}} and preserve the values of contour
integrals.

In the same vein, for a closed contour $\mathscr{C}(t)$ that encloses the set $U$,
and $f(\cdot)$ holomorphic across $U$, then we observe that
\begin{equation}
  \oint_\mathscr{C} f(z) dz = 0
\end{equation}

If, however, a closed contour $\mathscr{d}(t)$ enclosing $V$ contains
singularities of $f(\cdot)$, we see something different. In a simple case,
let $\gamma(t) = \exp(i t)$ and $f(z) = \frac{1}{z}$, we have $D\gamma(t) = i \exp(i t)$:
\begin{align}
  \oint_\gamma f(z) dz &= \oint \frac{1}{\exp(i t)} i \exp(i t) dt \\
  &= \oint i dt \\
  &= 2 \pi i
\end{align}

More generally, we have the first order of Cauchy's Integral Formula:
for $f(\cdot)$ holomorphic on $U$,
\begin{equation}
  2 \pi i f(a) = \oint_{\partial D} \frac{f(z)}{z - a} dz
\end{equation}
and higher orders,
\begin{equation}
  2 \pi i (D^nf)(a) = n! \oint_{\partial U} \frac{f(z)}{(z-a)^{n+1}} dz
\end{equation}
This result, encapsulating the idea that singularities carry information
in holomorphic functions, forms the core of a lot of powerful techniques.

\newthought{You might also note} that the contour integrals we've defined up
to here require contours parameterized as $\gamma(t) : \mathbb{R} \mapsto \mathbb{C}$.
This does not allow for improper integrals, even if we promote this definition
to the extended real and complex sets $\gamma(t) : \mathbb{R}_\infty \mapsto \mathbb{C}_\infty$,
since we need to distinguish ``how'' contours approach infinity.
However, we can develop improper contour integrals in the same way that
real improper integrals are developed: by taking limits to infinity of proper
ones.
For real functions,
\begin{equation}
  \int_\infty^\infty f(x) dx \equiv \lim_{\alpha \to -\infty, \beta \to \infty} \int_\alpha^\beta f(x) dx
\end{equation}
Similiarly, where $\mathscr{C}(t;r)$ defines a class of contours extending
outward to $\abs{z} = r$, we might want to integrate $f(z)$ along the contour
$\mathscr{C}(t;\infty) \equiv \lim_{r \to \infty} \mathscr{C}(t;r)$, and similiarly,
we define the integral by pulling the limit outside of it:
\begin{equation}
  \int_{\mathscr{C}(t;\infty)} f(z) dz \equiv \lim_{r \to \infty}  \int_{\mathscr{C}(t;r)} f(z) dz
\end{equation}
Where $\mathscr{C}(t;r)$ is a closed contour for all $r>R_{\mu}$ and some $R_\mu$, then we can apply
theorems about closed contours as normal.
In this case, points are considered encolsed by $\mathscr{C}(t;\infty)$ 
similiarly, if they are enclosed by all $r$ greater than some specifically
chosen $R_\mu$.

For example, let's consider a contour class $\mathscr{C}(t;r)$ which moves around
a semi-circle of radius $r$.
Specifically, let
\begin{equation*}
  \mathscr{C}(t;r) \equiv r \begin{cases}
    \exp(i \pi t) & 0 < t \leq 1 \\
    2 - r & 1 < t \leq 2 \\
    r - 2 & 2 < t \leq 3 \\
  \end{cases}
\end{equation*}
We note that any $w$ with $\Im(w)> 0$ is enclosed by $\mathscr{C}(t,\infty)$.
Additionally,
\begin{equation*}
  \lim_{r \to \infty} \abs{\mathscr{C}(t;r)} = \infty, \forall 0 < t \leq 1
\end{equation*}
so, for functions which go to zero at infinity (in all directions
in the complex plane), integrating along this contour, which wraps
all points in the positive-imaginary half-plane, is equivalent to integrating
along the real line.
This is a powerful property, which we can use to solve some otherwise hard
integrals.

