\section{Singularities and the Residue Theorem}

The generalized Taylor Series can be a useful tool when considering complex
functions.
In general,
\begin{equation}
  f(z) = \sum_{k \in \mathbb{Z}} c_k (z - z_0)^k
\end{equation}
for some $(c_k)$.
Note the inclusion of negative powers, which do not appear in many
real definitions of the Taylor series.
However, handling singularities of rational functions is a tool
that we would like from our complex calculus toolbox.
If $c_k = 0 \forall k < 0$, these definitions align,
and we call the function \emph{complex-analytic},
and we know that $f(\cdot)$ is holomorphic where that power series
converges\sidenote{This is because we can interchange the sum and integral
by linearity, and monomials are manifestly holomorphic around a zero.}.

However, we know that the function $z^{-1}$ is ``special,'' because, unlike
other monomials, it has an antiderivative that is not another monomial.
Here, this shows up if we apply our integral tricks to a function described by
a generalized series:
\begin{equation}
  \int_\gamma f(z) dz = \sum_k \int_\gamma c_k (z - z_0)^k = 2 \pi i c_{-1}
\end{equation}
Because of this property, we call $c_{-1}$ the \emph{Residue} of $f(\cdot)$ at $z_0$,
denoted $\Res[f(\cdot);z_0]$.

Another powerful property comes from the fact that 
we can continuously deform a contour that wraps several singularities
to ``partition'' isolated singularities, as in Figure~\ref{fig:residue_deform}.
This leads to the Residue Theorem,
\begin{equation}
  \int_\gamma f(z) dz = 2 \pi i \sum_k \Res[f(\cdot);z_k]
\end{equation}
where $\{z_k\}$ is the set of singularities of $f(\cdot)$.

\begin{figure} \label{fig:residue_deform}
  \includegraphics[width=0.8\textwidth]{figures/build/residue_deform.pdf}

  \caption{If a contour naturally encloses multiple but finitely many poles,
  we can deform it in a region that is still holomorphic into
  a set of contours which enclose either a single pole or a completely
  holomorphic region.}

\end{figure}

There are a few tricks\cite{beck2018} that we might like to have in our
toolbox for computing residues.
One relates to the differential version of Cauchy's Integral Formula:
for $f(z)$ with a pole at $w$,
\begin{equation}
  \Res[f(\cdot); w] = \frac{1}{(n-1)!} \lim_{z \rightarrow w} D^{n-1} ((z - w)^n f(z))
\end{equation}
If, however, the singularity at $w$ is removable,
\begin{equation}
  \Res[f(\cdot);w] = 0
\end{equation}

In general, we can use the residues of poles to characterize them,
since the actual values of functions around singularities are undefined.
This will be important when discussing Nyquist's Stability Criterion,
and other topics.

