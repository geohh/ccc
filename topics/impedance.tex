\section{Impedence matching, optimization, and an important
failure of complex calculus}

In this note, we will use the derivation of a conjugate impedance match
to discuss how to solve convex optimization problems with complex variables.

We consider a linear circuit, where a black box (modeled by its Thévenin
equivalent) delivers power to a load impedance.
We will treat the source impedance as fixed, and the load as
variable\sidenote{Building an impedance matching network is equivalent to changing
the effective impedance of the load as seen by the source, so this is a
reasonable view-from-the-top model.}.
Using this model, we will find the load impedance that enables maximum
power transfer from this linear model of a non-ideal source onto a load.

\begin{figure}
  \centering

  \begin{circuitikz}[american]
    \draw (0,2)
    to[V, V=$V_S$] (0,0);
    \draw (0,2)
    to[R, R=$Z_S$] (2,2)
    to[R, R=$Z_L$] (2,0)
    to[short] (0,0)
    ;
  \end{circuitikz}

  \caption{The circuit model to explore impedance matching --- we want to
  maximize power transfer from the source into the load impedance.}

\end{figure}

The \emph{Figure of Merit} we want to optimize is the time-averaged
power\cite{bigelow2020} %TODO: this breaks if I attempt to specifically cite chapter 8
dissipated over $Z_L$.
Importantly, it is only the real, resistive part an impedance that contributes
to the time-averaged power it removes from the circuit.
When we express the sinusoidal excitation in the frequency domain
\begin{align*}
  \expval{P} &= \frac{1}{T} \int_T v(t) i(t) \\
  &= \frac{1}{2} \Re(V(\omega) I^*(\omega)) \\
  &= \frac{1}{2} \Re \left( \left(V_S \frac{Z_L}{Z_S+Z_L} \right) \left( V_S \frac{1}{Z_S+Z_L} \right)^* \right) \\
  &= \frac{\abs{V_S}^2}{2} \frac{\Re(Z_L)}{\abs{Z_S+Z_L}^2}
\end{align*}

The first thing we'd like to ask is if the Figure of Merit is a holomorphism on
the feasible region of the optimization.
Using the Cauchy-Reimann equations, this is pretty direct:
we note the $P(Z_L)$ doesn't have an imaginary part, so $\pdv{v}{x} = \pdv{v}{y} = 0$ for
all points of interest.
Thus, for any region where the function is holomorphic, it is also constant,
and so questions of optimization are moot.
Thus, we will use the real notion of differentials in this convex optimization.
Where $Z_L = R_L + i X_L$,
\begin{equation}
  \pdv{P}{X_L} = - \abs{V_S}^2 R_L \frac{X_S + X_L}{(R_S+R_L)^2 + (X_S+X_L)^2} = 0 \implies X_L = - X_S
\end{equation}

\begin{equation}
  \pdv{P}{R_L} = - \frac{\abs{V_S}^2}{2} \frac{(R_S + R_L)^2 - 2 R_L(R_S + R_L)}{(R_S+R_L)^2} = 0 \implies R_L = R_S
\end{equation}

Thus, we find that to maximize the power that we deliver to a load, we want a \emph{conjugate impedance match}.
By deriving this, we exemplified one of the differences of complex and real
calculus:
\emph{strong relative optima}, the sort of optima that we seek within feasible
regions and in unconstrained optimization are not quite sensible to talk about
for holomorphic functions.
This can be related to the Cauchy Integral Formula:
since values of holomorphic functions at points can be found by weighted averaging around a boundary,
their parts can't achieve values more extreme than the boundary
(see Beck\cite{beck2018} for a proof that doesn't lean so deep into intuition).
If we want to find optima of holomorphic functions and their parts,
this will lead us in general towards boundary techniques.

