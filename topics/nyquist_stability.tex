\section{Nyquist's Stability Condition and the Argument Principle}
\label{sec:nyquist_stability}

In systems theory, we often care about \emph{stability}.
The concept of stability has several definitions, but in this case
the useful one is \emph{Bounded Input/Bounded Output (BIBO)} for
input/output systems.
In this model, we consider a system to be a map from input signals
$x(t)$\sidenote{Signals may loosely be thought of as maps from time to
some co-domain, however, many useful and intuitive results require
properties of Dirac's delta to prove. A more general notion of functions,
like a distribution theory,
is needed to include it, but will not be formalized here.}
to output signals $y(t)$, notated $H : x(t) \mapsto y(t) = H\{x(t)\}$.
This map may be \emph{Linear and Time-Invariant (LTI)}, in which case the
system can be completely described through its \emph{Laplace Transform}
$H(\s) : \mathbb{C} \mapsto \mathbb{C}$, through the relation
\begin{equation*}
  \mathcal{L}^{-1} \left\{ X(\s) \right\} \mapsto \mathcal{L}^{-1} \left\{ H(\s) X(\s) \right\}
\end{equation*}
where $X(\s) = \mathcal{L} x(t)$.
In this sense, the Laplace transform ``diagonalizes'' the action of LTI
systems.

For well-behaved systems, the Laplace Transform is usually a rational,
\emph{meromorphic} function
\begin{equation*}
  H(\s) = \frac{N(\s)}{D(\s)}
\end{equation*}
for $N(\cdot)$ and $D(\cdot)$ entire, so $H$ is entire except for points $p \in \{p_k\}$
where $D(p) = 0 \not= N(p)$.

BIBO stability for LTI systems can be stated simply as that all poles of
$H(\s)$ have negative real part.
Physically, the poles of a transfer function in the Laplace domain correspond
to unforced modes\sidenote{That is, solutions to the \emph{homogenous} problem}
of the underlying differential equation.
These ``modes'' take the form $x(t ; \s) = X(\s) \exp(\s t)$.
So, loosely speaking, if a system has an unforced mode with $\Re(\s) > 0$,
the signal value will increase without bound without being driven
by a corresponding unbounded input signal.

We would like to examine the stability of systems in negative feedback,
where instead of directly feeding the input signal $x$ into $H$, we drive
$e(t) = x(t) - K\{y(t)\}$ for another syste, the \emph{controller} $K$, a la 
Figure~\ref{fig:feedback_blocks}.
Specifically, we will consider the system with $H$ and $K$ LTI.
In this case, we can write the equations for a single mode, and generalize
by superposition
\begin{align}
  Y(\s) &= H(\s) (X(\s) - K(\s) Y(\s)) \\
        &= H(\s) X(\s) - H(\s) K(\s) Y(\s) \\
  Y(\s) (1 + H(\s) K(\s) ) &= H(\s) X(\s) \\
  G_{CL}(\s) \equiv \frac{Y(\s)}{X(\s)} &= \frac{H(\s)}{1+H(\s)K(\s)}
\end{align}
where $G_{CL}$ is the \emph{closed-loop gain} of the overall system.
This is called \emph{Black's Formula}.

\begin{figure}\label{fig:feedback_blocks}
  \tikzstyle{block} = [draw, fill=white, rectangle, 
    minimum height=3em, minimum width=6em]
  \tikzstyle{sum} = [draw, fill=white, circle, node distance=1cm]
  \tikzstyle{input} = [coordinate]
  \tikzstyle{output} = [coordinate]
  \tikzstyle{pinstyle} = [pin edge={to-,thin,black}]

  \begin{tikzpicture}[auto, node distance=2cm, >=latex]
    \node [input, name=input] {};
    \node [sum, right of=input,
%             pin={[pinstyle]below right:-},
%             pin={[pinstyle] above left:+},
            ] (sum) {};
    \node [block, right of=sum,
            node distance=2cm] (system) {System};
    \node [block, below of=system,
            node distance=2cm] (controller) {Controller};

    \draw [->] (sum) -- node[name=e, above] {$e$} (system);
    \node [output, right of=system,
            node distance=2cm] (output) {};

    \draw [draw,->] (input) -- node [name=r, above] {$r$} (sum);
    \draw [->] (system) -- node [name=y, above] {$y$}(output);
    \draw [->] (y) |- (controller);
    \draw [->] (controller) -| %node[pos=1.00] {$-$} 
        node [near end, left] {$u$ } (sum);
        
  \end{tikzpicture}

  \caption{High-level block diagram for a system in negative feedback.}
\end{figure}

\newthought{One especially useful} visualization aid is called the
\emph{Nyquist Plot}.
We note that the Fourier Transform is a line in the domain of the
Laplace Transform.
Instead of splitting the complex co-domain of
a transfer function into magnitude and phase,
as in the famous Bode Plot, we split it into real
and imaginary parts, and draw a parametric contour $H(i \omega)$ mapping
the forward-oscillatory axis $0 < \omega < \infty$.
Similiar to the conformality property of Mobius transforms,
we know that the image of a smooth contour under a meromorphic function
is another smooth contour, so long as we don't hit any of the singularities.
However, even these meromorphic transformations can lead to Nyquist contours
with topologically different structures, like the example in
Figure~\ref{fig:nyquist_plot}.
Unlike the Bode plot, with well-known asymtotic approximations suitable for
hand-plotting, generating Nyquist plots is more suited to computers,
but there's a powerful technique we can use to determine stability based 
on this visualization.

\begin{figure}\label{fig:nyquist_plot}
  \includegraphics{figures/build/nyquist_plot.pdf}

  \caption{The open-loop system $H(\s) = \frac{(\s-2)}{(\s-(-1+i))(\s-(-1-i))(\s-1)}$ ,
  and feedback factor $k=3$ create a closed-loop system with a different topology of
  Nyquist contour --- note the winding, as well as the reduced overall phase.}

\end{figure}

\newthought{Though many systems theory} references make other arguments to
justify Nyquist's stability criterion, I think there's some useful intuition
to be gotten out of using Cauchy's \emph{Argument Principle}.
To understand the Augument Principle, we will start from one of the most
famous results of univariate complex analysis --- the Residue Theorem.
We can state this result as, for a meromorphism $f(\cdot)$ and simple, closed
path $\gamma$ that avoids all of the finitely many singularities of $f$,
\begin{equation}
  \oint_\gamma f(z) dz = 2 \pi i \sum_k \Res(z_k;f(\cdot))
\end{equation}
For further discussion of this valuable result, see the theory chapter.

Logarithms have some nice properties, like monotonicity\sidenote{
Monotonicity means optimization problems are invariant.}, and
linearizing functions that would otherwise include mixers.
A valuable object to consider is the \emph{logarithmic derivative}
\begin{equation}
  D \log(f) = \frac{Df}{f}
\end{equation}
This form has a nice identity for products of functions,
\begin{equation}
  \frac{D(fg)}{fg} = \frac{Df}{f} + \frac{Dg}{g}
\end{equation}
Where a meromorphic $f$, with finitely many poles and zeros, is
defined in terms of a holomorphic $g$,
\begin{equation*}
  f(z) \equiv \prod_k (z-p_k)^{m_k} g(z)
\end{equation*}
\begin{equation*}
  \frac{Df}{f} = \frac{Dg}{g} + \sum_k m_k (z-p_k)^{-1}
\end{equation*}

Since $g$ is holomorphic and non-zero in the domain of interest,
$\frac{Dg}{g}$ is as well and will fall out of closed contour integrals.
The other parts can be directly evaluated with Cauchy's Integral Formula,
and interpreted as counting orders of poles and zeros.
This results can be stated then, where $Z$ is the sum of the multiplicities of the
zeros, and $P$ is the sum of multiplicities of the poles of $f(\cdot)$,
\begin{equation}
  \oint_\gamma \frac{Df(z)}{f(z)}dz = 2 \pi i (Z - P)
\end{equation}

This closed-loop coutour integral can be useful to understand
as a winding number around the origin where positive increments correspond
to counter-clockwise winding.
Specifically, we note that we can write a complex differntial in polar coordinates
\begin{equation}
  dz = (\partial_r + \partial_\theta) \{r \exp(i \theta)\} = \exp(i \theta) (dr + i r d\theta)
\end{equation}
\begin{equation}
  \frac{dz}{z} = d[\log(r)] + i d \theta
\end{equation}
Since there is no net change in $r$ over a closed contour,
the only significant part is angular, and a contour will pick up
$2 \pi i$ per winding around the origin.
This can be notated using a function $N(\gamma, w)$ which returns winding
numbers of a curve about a point:
\begin{equation}
  \int_\gamma \frac{dz}{z} = \int_\alpha^\beta \frac{D\gamma(t)}{\gamma(t)} dt = 2 \pi i N(\gamma, 0+0i)
\end{equation}
However, we can also find winding numbers graphically.
Counting the number of times a curve $\gamma$ intersects a ray from $w$ to infinity,
in any direction, where counter-clockwise crossings increment and clockwise
crossings decrement, will also return $N(\gamma, w)$.
%TODO: visual example of this
So, we can also write the argument principle as $N(\gamma,0) = Z - P$.
One consideration in this representation, however, is that where $Z$ and $P$ are
measured within the ``original'' contour $\gamma$, $N$ is measured within the ``deformed''
contour $f(\gamma)$.

In this lens, the Nyquist stability criterion can be justified using a contour
class $\mathscr{N}(t;r)$ of semicircles of radius $r$ with the straight edge along
the imaginary axis, and the envolped region in the positive-real half-plane.
In the limit as $r \to \infty$, this contour will envelop all poles and zeros in the
RHP.
However, all of the points that are not on the imaginary axis are infinitely
far from the origin, so if the system is overall low-pass\sidenote{Most
real systems are band-limited in some way, so this is not too un-reasonable of
an assumption.} that part of the contour has not real effect on the integral.
In the nyquist plot visualization, this means that we don't need to graph along it,
at the cost of a potential sudden change in phase at the origin.
We simply draw the contour that results from deforming the Fourier line according to
$H(\s)$ and count winding numbers.

\newthought{We want to use} the Nyquist Stability Criterion to specifically check the
stability of feedback systems.
In short, if the counting argument above requires the the denominator of the closed
loop transfer function has RHP zeros, then the system is unstable.
One might intuitively find a ``hole'' in this argument, as the $H(\s)$ in the numerator
of Black's formula preserves poles of the open-loop function.
However, where $H(\s) = (\s - p)^{-1} H'(\s)$ and $H'(\s)$ well-behaved around $p$,
\begin{align}
  T(\s) &= \frac{H(\s)}{1+K(\s)H(\s)} \\
  &= \frac{(\s - p)^{-1} H'(\s)}{1+(\s - p)^{-1}K(\s)H'(\s)} \\
  &= \frac{H'(\s)}{(\s - p) + K(\s)H'(\s)}
\end{align}
which is well-behaved around $p$ if and only if $K(\s)H'(\s)$ is well-behaved there.
This leads us to an important expression: the \emph{open-loop gain} $K(\s)H(\s)$.
Poles in the closed-loop gain are associated with points where the open-loop gain
equals $-1$, or, for constant feedback $K(\s) = k$, points where $H(\s) = -\frac{1}{k}$.
For that reason, the winding number of $H(\mathscr{N}(t;\infty))$ around $-\frac{1}{k}$
plus the number of unstable poles in the loop gain $N+P = Z$, the number of zeros
in the denominator of the closed-loop gain.
Using this, we can look at a plot like Figure~\ref{fig:nyquist_plot} and determine a
range of feedback values for which the closed-loop sytem is stable or not.

