\section{Contour Techniques for some hard integrals}

An acknowledgement to Hitoshi Murayama, whose excellent course
notes\cite{murayama:contour_notes} form the basis of this section.

In a standard table of integrals\cite{wiki:lists_of_integrals},
you'll find some known definite integrals along the real line
are not associated with a known closed-form antiderivative, so
they must not have been evaluated the conventional way (Fundamental
Theorem of Calculus).
For some of these, the ``un-conventional way'' is to promote
the real function to one on the complex plane, and integrate
it along the real line, and some other path to make a closed
contour.
Then, we can apply the Cauchy Integral Formula.
\begin{equation}
  2 \pi i f(a) = \oint_\gamma \frac{f(z)}{z-a} dz
\end{equation}

\newthought{There's a bit more nuance} to the structure of
the transform $z \mapsto \frac{1}{z} = \frac{\bar{z}}{\abs{z}^2}$ that we should establish
before we go this route.
If we think of $\mathbb{C}$ as a 2-dimensional vector space,
we note that for $z \not= 0$, $z = x + i y$ and $\bar{z} = x - i y$ are linearly
independant.
We can define differential operators with respect to these
variables which are consistent with complex differentiation
as normally conceptualized:
\begin{align}
  \partial = \frac{\partial_x - i \partial_y}{2} &&
  \bar{\partial} = \frac{\partial_x + i \partial_y}{2}
\end{align}
Some interesting relationships:
\begin{align}
  \partial z &= \frac{1}{2}(\partial_x x - i \partial_y x + i \partial_x y + \partial_y y) = 1 \\
  \bar{\partial} \bar{z} &= \frac{1}{2} \left( \partial_x x + i \partial_y x - i \partial_x y + \partial_y y \right) = 1 \\
  \partial \bar{z} &= \frac{1}{2} \left( \partial_x x - i \partial_y x - i \partial_x y - \partial_y y \right) = 0 \\
  \bar{\partial} z &= \frac{1}{2} \left( \partial_x x + i \partial_y x + i \partial_x y - \partial_y y \right) = 0 \\
  [\partial, \bar{\partial}] = \partial \bar{\partial} - \bar{\partial} \partial &=
  \frac{1}{4} \left( \partial_x^2 + \partial_y^2 - \partial_x^2 - \partial_y^2 \right) = 0 \\
  \Delta = \partial_x^2 + \partial_y^2 = 4 \partial \bar{\partial}
\end{align}
where $\Delta$ is the \emph{Laplacian} on the plane.
Effectively, this is ``factoring'' the Laplacian.

Consider the Poisson equation that relates charge and electric potential,
for a point charge:
\begin{equation} \label{eqn:poisson-charge}
  \Delta \left( \frac{1}{4 \pi} \log r^2 \right) = \delta^2(\vec{r})
\end{equation}
In 2 dimensions, we can transform into the complex plane:
\begin{equation}
  \frac{1}{\pi} \bar{\partial} \partial \log(\bar{z} z) = \delta^2(\vec{r})
\end{equation}
Noting that $\log(ab) = \log(a) + \log(b)$, we can do the $\partial$ derivative
and get
\begin{equation}
  \frac{1}{2 \pi} \bar{\partial} \frac{1}{z} = \delta^2(\vec{r})
\end{equation}

Since this conjugate-derivative has a non-trivial functional structure,
we note that the function $f(z) = \frac{1}{z}$ hides a dependance on $\bar{z}$.
It is exactly this local dependance on a complementary variable that the closed
contour integral ``picks up'' value from.
We must note, however, that equation~\ref{eqn:poisson-charge} is a statement of natural law,
not mathematical truth, so this discussion cannot be treated as generally rigorous.

\newthought{As an aside:} at first glance, Cauchy's Integral Formula appears to
``reduce dimensionality'' in complex-valued data, as it uses only values
on the boundary of a space to compute arbitrary values within the space.
However, any attempt to create a systematic scheme for writing 2d data in terms
of boundary data for holomorphic functions will be stymied by the limits on the
structure of holomorphic functions.
This is exactly because of the ``basis'' structure for the complex plane that
$z$ and $\bar{z}$ share.
Holomorphisms are functions in only a single ``dimension'' of information,
so it is natural that they can be written in terms of boundary values.
Cauchy's Integral Formula, thus, can be thought of as a basis transformation
to a 1-dimensional domain that contains all of the ``information'' in a
holomorphism.

\newthought{We can relate} some integrals that are tractable with real techniques
to contour integrals to develop a class of integration techniques in the complex
plane.
For example, consider
\begin{equation}
  f(x) = \frac{1}{x^2 + a^2} = D_x \left( \frac{1}{a}\arctan(\frac{x}{a}) \right)
\end{equation}
Since the antiderivative exists, we can integrate this directly
\begin{align}
  \int_{-\infty}^\infty \frac{dx}{x^2 + a^2}
  &= \lim_{\alpha \to \infty, \beta \to -\infty} \int_\beta^\alpha \frac{dx}{x^2 + a^2} \\
  &= \lim_{\alpha \to \infty, \beta \to -\infty} \frac{1}{a} \left( \arctan(\frac{\alpha}{a}) - \arctan(\frac{\beta}{a}) \right) \\
  &= \frac{1}{a} \left( \frac{\pi}{2} - \left( - \frac{\pi}{2} \right) \right) \\
  &= \frac{\pi}{a}
\end{align}


\begin{figure}
  \begin{tikzpicture}
    \begin{axis} [
      axis lines=middle, axis on top,
      xmin=-2,
      xmax=2,
      ymin=-2,
      ymax=2,
      zmin=-4,
      zmax=4,
      miter limit=1,
      ]
      \addplot3 [
        smooth,
        surf,
        domain=-2:2,
        y domain=-2:2,
        samples = 30,
        samples y = 30,
        restrict z to domain*=-4:4
      ] {
        1/(x^2 - y^2 + 1)
      };
    \end{axis}
  \end{tikzpicture}
\end{figure}


