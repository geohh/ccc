\section{Inverse Laplace Transforms and the Residue Theorem}

Despite the ubiquity of the Laplace transform in the analysis of
circuits and control systems, it is rare to directly
compute its inverse.
More commonly, one will tabulate known forms of the forward, unilateral transform,
\begin{equation}
F(\s) = \mathcal{L}\{f(\cdot);\s\} = \int_0^\infty \exp(\s t) f(t) dt
\end{equation}
and combine them with some algebra and a healthy dose of intuition
to convert signals back from the Laplace to time domain.
The Laplace transform maps functions of a real parameter $t$ to 
functions of a complex parameter $\s$, so one might naively think that
there is more information in $F(\s)$, and it is impossible to reconstruct
$f(t)$.
In a very literal sense, this is true.
For example, consider
\begin{equation*}
  f(t) = \exp(t)
\end{equation*}
\begin{equation*}
  g(t) = \begin{cases}
    f(t) & t > 0 \\
    0 & t = 0 \\
  \end{cases}
\end{equation*}
These ARE different functions, but the integral to compute the forward transform
doesn't care about the single-point deformation.
Loosely speaking, if the difference between two functions is not ``measurable under
integration'' then their Laplace transforms are the same.
However, we know that the laplace transforms of LTI systems %TODO: cite or prove
are rational with finitely many poles and zeros.
As we know from the Residue Theorem, and Cauchy Integral Formula,
information about meromorphic functions is contained at the poles and
on the boundary, which is no more information-dense than a real function.
Intuitively, we should have just enough information in the Laplace transform
of a well-behaved signal to re-construct the original signal.
So, how?

\newthought{First, think about} a signal that has been ``modulated''
in Laplace space such that it is integrable on $t > 0$: let
\begin{equation}
  \varphi(t) = \begin{cases}
    f(t) \exp(-\sigma t) & t \geq 0 \\
    0 & t < 0 \\
  \end{cases}
\end{equation}
where $\sigma$ is chosen to be greater than the real part of any poles.
By Parseval's, it has a Fourier transform which we can write in terms
of the Laplace transform of $f$.
\begin{equation}
  \Phi(\omega) = \int_0^\infty f(t) \exp(-\sigma t) \exp(-i \omega t) dt = F(\sigma + i \omega)
\end{equation}
We can re-construct $\varphi(t)$ with an inverse Fourier transform,
\begin{equation}
  \varphi(t) = \int_{-\infty}^{\infty} F(\sigma + i \omega) d \omega
\end{equation}
which looks an awful lot like an inverse Laplace transform. Hooray!
But this line integral in the complex plane is hard
for practical systems. Hooray??
Specifically, we can present the inverse Laplace transform as the
\emph{Fourier-Mellin integral}
\begin{equation}
  f(t) = \mathcal{L}^{-1}\{F(\s)\} = \frac{1}{2 \pi i} \int_{\sigma - i \infty}^{\sigma + i \infty} \exp(\s t) F(\s) d \s
\end{equation}

We're presented with two natural questions:
\begin{enumerate}
    \item How do we choose $\sigma$?
    \item and how do we get useful results out of this integral?
\end{enumerate}
Fortunately, the answers to both of these questions can be found
in the Residue theorem.

\newthought{In general, for real systems,} there are inherent, physical limits
on the bandwidth of the system.
This is natural, as physical systems tend to have some resistance to rapid
state changes: e.g. capacitance in electronics and spring force in mechanics.
This manifests itself in the Laplace transform as being \emph{zero at infinity},
or, $\lim_{\abs{\s} \to \infty} F(\s)= 0$ for all directions of approach to infinity.
A transfer function with more finite poles than zeros may loosely be considered to have
extra zeros at infinity.

Where we have this bandwidth limit, we can extend the Fourier-Mellin integral into a closed
contour, and use all of the powerful results about closed contour integrals
that we have in our toolbox.
This is sometimes called the \emph{Bromwich contour, and can be seen in Figure~\ref{fig:fourier_mellin}}.
For the part of the contour which is not part of the Fourier-Mellin line of integration, $\mathscr{B}_1$, $\abs{\s} = r$,
so in the limit as $r \to \infty$, $H(\mathscr{B}_1) = 0$.
\begin{equation}
  \int_{\mathscr{B}_1} F(\s) d\s = \int 0 d\s = 0
\end{equation}
Since this closed contour integral is equivalent to the integral we want to evaluate,
we now need to pick the theorem out of our contour integral toolbox that suits this
problem.
System functions in the laplace domain are generally meromorphic, and may have many poles,
so the Residue theorem will be a good fit.
A reminder: the residue theorem can be stated that for a simple, closed contour $\gamma$
\begin{equation}
  \oint_\gamma F(\s) d\s = 2 \pi i \sum_k \Res(F(\s), p_k)
\end{equation}
So, taking the limit as our contours extend to match the Fourier-Mellin,
\begin{equation}
  f(t) = \lim_{r \to \infty} \frac{1}{2 \pi i} \oint_{\mathscr{B}(\cdot;r)} F(\s) \exp(\s t) d\s = \sum_k \Res(F(\s) \exp(\s t); p_k)
\end{equation}
Note that this also removes the dependance on explicitly choosing a value of $\sigma$:
so long as all poles of the system function $F(\s)$ are contained by the
implicit choice, the original $f(t)$ will be correctly recovered.

\begin{figure}\label{fig:fourier_mellin}

  \includegraphics[width=\textwidth]{figures/build/fourier_mellin.pdf}

  \caption{The Fourier-Mellin integral, along the line $\mathcal{M}$,
  can be extended to the closed Bromwich contour $\mathscr{B}(\cdot;r=4)$.
  The system function in this example, $H(\s) = \frac{\s - 1}{(\s - 1 + i) (\s - 1 - i) (\s + 2)}$,
  has three finite poles and one finite zero, so it must go to zero at infinity.}

\end{figure}

For example, let's actually take the inverse Fourier transform of
the system function
\begin{equation*}
  H(\s) = \frac{(\s - 1)}{(\s - 1 + i) (\s - 1 - i) (\s + 2)}
\end{equation*}
This function has three simple poles, $p_k = \begin{pmatrix} 1 \pm i, & 2 \end{pmatrix}$.
The well-known formula for the residue of a pole simplifies greatly for first order poles:
\begin{equation}
  \Res[H(\s);\s = w] = \lim_{\s \to w} (\s - w) H(\s)
\end{equation}
$\exp(\s t)$ is entire, so we don't worry about it.
\begin{align*}
  \Res(H(\s)\exp(\s t);\s=1+i)
  &= \lim_{\s \to 1+i} \exp(\s t) \frac{(\s - 1)}{(\s - 1 + i) (\s + 2)} \\
  &= \exp((1+i) t) \frac{1}{6+2i} \\
  &= \exp((1+i) t) \frac{3-i}{20}
\end{align*}
\begin{align*}
  \Res(H(\s)\exp(\s t);\s=1-i)
  &= \lim_{\s \to 1-i} \exp(\s t) \frac{(\s - 1)}{(\s - 1 - i) (\s + 2)} \\
  &= \exp((1-i) t) \frac{1}{6-2i} \\
  &= \exp((1-i) t) \frac{3+i}{20}
\end{align*}
\begin{align*}
  \Res[H(\s)\exp(\s t);\s=-2]
  &= \lim_{\s \to 2} \exp(\s t) \frac{(\s - 1)}{(\s - 1 - i) (\s - 1 + i)} \\
  &= -\exp(-2 t) \frac{3}{10}
\end{align*}
So, overall,
\begin{equation*}
  f(t) = \sum_k \Res[H(\s) \exp(\s t);\s=p_k] = \frac{e^t}{10}(3 \cos(t) + \sin(t)) - \frac{3}{10} e^{-2t} 
\end{equation*}

\begin{figure}

  \includegraphics[width=\textwidth]{figures/build/laplace_numerical_vs_residue.pdf}

  \caption{Numerical (Talbot algorithm) vs residue theorem analytic
  solutions to the inverse Laplace transform for the example.
  Note the discrepance close to $t=0$.}
\end{figure}

This result can be verified, both by comparing it to the results of known
numerical approximations to the inverse Laplace transform,
and by directly evaluating the easier, forward Laplace transform
of the analytic result.

