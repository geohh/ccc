#! julia --project

using PGFPlots

Nside = 8
Ndots = Nside^2
z=[1:Ndots;1:Ndots;]

s = sqrt(2)/2 - 0.1

xs = range(-s, s; length=Nside)
ys = xs*im
zs = xs .+ ys'

one_over_zs = map(z -> 1/z, zs)

graph_xs = [reshape(real(zs), (Ndots));reshape(real(one_over_zs), (Ndots));]
graph_ys = [reshape(imag(zs), (Ndots));reshape(imag(one_over_zs), (Ndots));]

unit_circle = exp.(im*2*pi*[1:100;]/100)

ax = Axis([PGFPlots.Plots.Scatter(graph_xs, graph_ys, z),
           PGFPlots.Plots.Linear(real(unit_circle), imag(unit_circle), style="thin, black", mark="none"),
          ],
          xmin=-4, xmax=4, ymin=-4, ymax=4
         )

save("figures/build/one_over_z.tex", ax; include_preamble=false)
save("figures/build/one_over_z.pdf", ax)

