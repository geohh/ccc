using Plots, LaTeXStrings
gr()

func_cart(f) = (x, y) -> f(x + im*y)

H(s) = (s-2)/(s-2+im)/(s-2-im)/(s+1)
H_cart = func_cart(H)
mag_H_cart(x,y) = abs(H_cart(x,y))
sig = 2.5
ommax = 4
oms = [range(-ommax,ommax;length=200);]
ts = [range(pi/2,3*pi/2;length=200);]
brom = [sig .+ im .* oms; sig .+ ommax .* exp.(im.*ts);]
mell = sig .+ im .* oms

xs = ys = [range(-4,4;length=200);]

p = plot(xs,ys,mag_H_cart; st=:surface, zlim=(-0,0.8), xlabel=L"\Re{s}", ylabel=L"\Im{s}")
plot!(real.(brom), imag.(brom), abs.(H.(brom)); label=L"\mathscr{B}(\cdot;r=4)")
plot!(real.(mell), imag.(mell), abs.(H.(mell)); label=L"\mathscr{M}(\cdot)")

savefig(p, "figures/build/fourier_mellin.pdf")

