using Plots
gr()

using QuadGK

g(t) = sin(t) + sin(3*t)

ts = [range(0,20;length=2000);]
xs = [range(-5,5;length=200);]
ys = [range(0,3;length=100);] .+ 0.001
boundary_data = g.(xs)

function u(x,y) 
  integral, _ = quadgk(t -> g(t)*y/( (x-t)^2 + y^2 ), minimum(xs)*2, maximum(xs)*2)
  return integral/pi
end
p = surface(xs, ys, u)
plot!(xs, zeros(length(xs)), boundary_data;
     color="blue",
     label="\$g(x)\$",
    )

savefig(p, "figures/build/halfplane_laplace.pdf")

