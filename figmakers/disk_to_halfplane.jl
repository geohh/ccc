using Plots
gr()

rsq(x,y) = abs(x^2) + abs(y^2)
rsq(z) = abs(z)^2

phis = [range(0,2pi;length=100);]
unit_circle = 1.0 .* exp.(im .* phis)

xs = ys = range(-1,1;length=20)
unit_disk = [x+im*y for x in xs, y in ys if abs(x+im*y)<1]

inner_circle_Rs = [0.2:0.2:0.8;]
inner_circles = [R .* exp.(im .* phis) for R in inner_circle_Rs]


cayley(z) = (z+im)/(im*z+1)

ax_line = cayley.(unit_circle)
inner_lines = [cayley.(inner_circle) for inner_circle in inner_circles]

disk_image = cayley.(unit_disk)

#  upper = plot(real.(unit_circle), imag.(unit_circle))
#  [plot!(real.(inner_circle), imag.(inner_circle); c="gray", line=:dash) for inner_circle in inner_circles]
#
#  lower = plot(real.(ax_line), imag.(ax_line))
#  [plot!(real.(inner_line), imag.(inner_line); c="gray", line=:dash, label=nothing) for inner_line in inner_lines]
#
upper = surface(real.(unit_disk), imag.(unit_disk),
                abs.(unit_disk).^2,
                #  fill_z=abs.(unit_disk),
                aspect_ratio=:equal
               )

lower = surface(real.(disk_image), imag.(disk_image),
                abs.(unit_disk).^2,
                #  fill_z=abs.(unit_disk),
                aspect_ratio=:equal
              )

p = plot(upper, lower)

savefig(p, "figures/build/disk_to_halfplane.pdf")
