using Plots, LaTeXStrings
gr()
using QuadGK

r(x,y) = sqrt(x^2 + y^2)
arg(z) = atan(real(z), imag(z))

gouter(phi) = sin(3*phi)
Router = 1
phis = [range(0,2pi;length=100);]
boundary=Router .* exp.(im .* phis)
boundary_data = gouter.(phis)

poisson_kernel(r, th) = (1-r^2)/(1-2*r*cos(th)+r^2)
function u(z)
  r = abs(z)
  th = arg(z)
  o, err = quadgk(t -> poisson_kernel(r, th-t)*gouter(t), -pi, pi)
  return o/(2pi)
end

u_cart(x,y) = u(x+im*y)

xs = ys = [range(-1,1;length=20);]

zs = [x + im*y for x in xs, y in ys if r(x,y) < Router]

p = plot(real.(zs), imag.(zs), u.(zs);
         st=:surface,
         xlabel=L"x",
         ylabel=L"y",
         linetype=:surface
        )
plot!(real.(boundary), imag.(boundary), boundary_data;
      legendentry=L"g(\phi)",
     )

savefig(p, "figures/build/poisson_kernel.pdf")

