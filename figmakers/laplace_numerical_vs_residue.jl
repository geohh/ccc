using Plots, InverseLaplace

F(s) = (s-1)/(s+2)/(s-1-im)/(s-1+im)

Nterms = 64
ft = Talbot(F, Nterms)

fa(t) = exp(t)/10 * (sin(t) + 3*cos(t)) - 3/10 * exp(-2t)

ts = [0:0.05:5.0;]

p = plot(ts, real.(ft.(ts)); label="Numeric ($(Nterms) terms)")
plot!(ts, fa.(ts); label="Analytic")

savefig(p, "figures/build/laplace_numerical_vs_residue.pdf")

