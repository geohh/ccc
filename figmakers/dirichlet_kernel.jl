using Plots
pgfplotsx()

ns = [1, 4, 7]

D(n) = x -> sin((n+1/2)x)/(sin(x/2))

xs = [range(2pi - 4, 2pi + 4; length=1000);]

p = plot()
[plot!(xs, D(n); label="\$D(x;$(n))\$") for n in ns]

savefig(p, "figures/build/dirichlet_kernel.pdf")

