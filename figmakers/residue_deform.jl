#! julia --project

using PGFPlots

poles = [-1,1] .* 1.2
pole_plot = PGFPlots.Scatter(real(poles), imag(poles), mark="x")

ts = [range(0,2*pi,length=500);]
main_circle(t) = 1.5*exp(im*t)
main_contour = main_circle.(ts)

deformation_arc(t) = cos(t) - im*5*sin(t) + im*sin(5/4*pi)

function deformed_contour(t)
  if (t < 5/4*pi) || (t > 7/4*pi)
    main_circle(t)
  else
    deformation_arc(t)
  end
end
Gamma = deformed_contour.(ts)

ax1 = Axis([
            pole_plot,
            PGFPlots.Linear(real(main_contour), imag(main_contour), mark="none", style="-latex", legendentry="Original Contour"),
           ],
          )

ax2 = Axis([
            pole_plot,
            PGFPlots.Linear(real(Gamma), imag(Gamma), mark="none", style="-latex", legendentry="Deformed Contour"),
           ],
          )

g = PGFPlots.GroupPlot(1,2, groupStyle="horizontal sep = 1.75cm, vertical sep = 1.5cm")
push!(g, ax1)
push!(g, ax2)

save("figures/build/residue_deform.pdf", g)

