#! julia --project

N = 50
Z0 = 10

using PGFPlots

z_to_gamma(z) = (z - 1)/(z + 1)

ts = [1:N*Z0;] ./ N
zs_R = ts
zs_L = ts * im
zs_C = ts * (-im)

vertical_line(x) = x.+[ts*im;-ts*im]
horizont_line(y) = (y*im).+ts

gridlines_v = [vertical_line(x) for x in 1:(Z0-1)]
gridlines_h = [horizont_line(y) for y in -(Z0-1):(Z0-1)]
gridlines = [gridlines_v; gridlines_h;]

gamma_grid = map(arr -> z_to_gamma.(arr), gridlines)

gammas_R = z_to_gamma.(zs_R)
gammas_L = z_to_gamma.(zs_L)
gammas_C = z_to_gamma.(zs_C)

z_plot = Axis([[Plots.Linear(real(zs_R), imag(zs_R), legendentry=L"Increasing $\Re(z)$", style="-latex,very thick", mark="none"),
                Plots.Linear(real(zs_L), imag(zs_L), legendentry=L"Increasing $\Im(z)$", style="-latex,very thick", mark="none"),
                Plots.Linear(real(zs_C), imag(zs_C), legendentry=L"Increasing $-\Im(z)$", style="-latex,very thick", mark="none")];
               [Plots.Linear(real(zs), imag(zs), style="thin, gray, dashed", mark="none") for zs in gridlines];
              ],
              legendPos="north east",
              title="Normalized Impedence",
              xlabel=L"\Re(z)",
              ylabel=L"\Im(z)",
             )

gamma_plot = Axis([[Plots.Linear(real(gammas_R), imag(gammas_R), legendentry=L"Increasing $\Re(z)$", style="-latex,very thick", mark="none"),
                    Plots.Linear(real(gammas_L), imag(gammas_L), legendentry=L"Increasing $\Im(z)$", style="-latex,very thick", mark="none"),
                    Plots.Linear(real(gammas_C), imag(gammas_C), legendentry=L"Increasing $-\Im(z)$", style="-latex,very thick", mark="none")];
                   [Plots.Linear(real(zs), imag(zs), style="thin, gray, dashed", mark="none") for zs in gamma_grid];
                  ],
              legendPos="north east",
              title="Reflection coefficient",
              xlabel=L"\Re(\Gamma)",
              ylabel=L"\Im(\Gamma)",
             )

g = GroupPlot(1,2, groupStyle = "horizontal sep = 2.00cm, vertical sep = 2.00cm")
push!(g, z_plot)
push!(g, gamma_plot)

save("figures/build/Z_to_Gamma.tex", g; include_preamble=false)
save("figures/build/Z_to_Gamma.pdf", g)

