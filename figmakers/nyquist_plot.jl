#! julia --project

using PGFPlots

H(s) = 1000/(s+5)^3
k = 1
G(s) = H(s)/(1+k*H(s))

freqs = im .* (exp10.(range(-8, 5, length=1000)))

nyquist_path_ol_pos = H.(freqs)
nyquist_path_ol_neg = H.(-freqs)
nyquist_path_cl_pos = G.(freqs)
nyquist_path_cl_neg = G.(-freqs)

unity_circle = exp.(im*2*pi/100 .* range(1,100,step=1))

ax = Axis([
           Plots.Linear(real.(nyquist_path_ol_pos), imag.(nyquist_path_ol_pos), style="blue", mark="none", legendentry="Open Loop"),
           Plots.Linear(real.(nyquist_path_ol_neg), imag.(nyquist_path_ol_neg), style="blue", mark="none"),
           #  Plots.Linear(real.(nyquist_path_cl_pos), imag.(nyquist_path_cl_pos), style="red", mark="none", legendentry="Closed Loop"),
           #  Plots.Linear(real.(nyquist_path_cl_neg), imag.(nyquist_path_cl_neg), style="red", mark="none"),
           Plots.Linear(real.(unity_circle), imag.(unity_circle), style="dashed, gray", mark="none"),
           #  Plots.Scatter(0,0, mark="x", markSize=10),
          ],
          legendStyle="at={(1.05,1.0)}, anchor=north west",
         )

save("figures/build/nyquist_plot.pdf", ax)
