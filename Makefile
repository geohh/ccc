LATEX_COMMAND=latexmk -pdf -shell-escape
MASTER_FILE=main

FIGMAKERS_JL=$(wildcard figmakers/*.jl)
FIGURES_PDF=$(subst figmakers,figures/build,$(FIGMAKERS_JL:.jl=.pdf))

TOPICS_TEX=$(wildcard topics/*.tex)

JULIA=julia --project

.PHONY : all clean cleantex $(MASTER_FILE).pdf

main.pdf: $(FIGURES_PDF) $(MASTER_FILE).tex $(TOPICS_TEX)
	$(LATEX_COMMAND) $(MASTER_FILE).tex

all: main.pdf

cleantex:
	latexmk -c

clean: cleantex
	rm figures/build/*.pdf
	
figures/build/%.pdf: figmakers/%.jl
	$(JULIA) $<

